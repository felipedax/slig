<?php

namespace app\daos;

abstract class DAO
{
    protected $conn;

    public function __construct()
    {
        $this->conn = Connection::getInstance();
    }

    // retorna todas colunas da tabela
    public function all()
    {
        $sql = "select * from {$this->table}"; // tabela referenciada na lasse que extende DAO
        $all = $this->conn->query($sql);
        $all->execute();

        return $all->fetchAll();
    }
}
