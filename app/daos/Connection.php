<?php

namespace app\daos;

use PDO;

class Connection{
  private static $conn = null;

  public static function getInstance()
  {
    if (self::$conn !== null) {
      return self::$conn;
    } else {
      $dbdrive = 'mysql';
      $hostname = 'localhost';
      $database = 'comar';
      $username = 'felipe';
      $password = '';

      self::$conn = new PDO($dbdrive . ':host=' . $hostname . ';dbname=' . $database . ';charset=utf8', $username, $password);
      self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      return self::$conn;
    }
  }
}
