<?php

namespace app\traits;

use app\models\Load;

// trait para utilização do twig
trait View
{
  protected $twig;

  // função que sempre será chamada pelas controllers
  protected function view($view,$data)
  {
    $this->load();

    // carrega o arquivo template html
    // transforma string do path diretorio.arquivo em diretorio/arquivo.html caso neessario
    $template = $this->twig->loadTemplate(str_replace('.','/',$view).'.html');

    // envia variavel para o template
    return $template->display($data);
  }

  protected function load()
  {
    $this->twig();

    $this->functions();
  }

  protected function twig()
  {
    // caminho dos templates html das views
    $loader = new \Twig_Loader_Filesystem('../app/views');

    $this->twig = new \Twig_Environment($loader, array(
      // 'cache' => '../app/views/cache',
      'debug' => true
    ));
  }

  // funções específicar para trabalhar com twig
  protected function functions()
  {
    // retorna o arquivo das funçoes do template do twig
    $functions = Load::file('/app/functions/twig.php');

    // adiciona para o template as funções de /app/functions/twig.php
    foreach($functions as $function){
      $this->twig->addFunction($function);
    }
  }
}
