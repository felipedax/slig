<?php

require "../bootstrap.php";

/*
* Sem controllers
*/

// localhost/
// $app->get('/', function(){
//     echo 'home';
// });

// localhost/admin/login
// $app->group('/admin', function() use($app){
//     $app->get('/login',function(){
//         echo 'login';
//     });
// });

// localhost/update/user/5
// $app->get('/update/user/{id}', function(Request $request, Response $response, array $args){
//     dd($args['id']);
// });


/*
* Com controllers
*/

//$app->get('/rota','namespace\ClasseController:Metodo');
$app->get('/','app\controllers\HomeController:index');
$app->get('/show/{nome}','app\controllers\HomeController:show');

$app->run();
