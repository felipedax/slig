:scissors:

<div align="center">
  <img width="200" src="public/assets/imgs/slig.png">
</div>

# Template Slig = Slim + Twig
## O que é?
Meu template padrão para projetos php. No momento ele é idêntico ao apresentado pelo [Alexandre Cardoso](#https://www.udemy.com/twig-com-slim-framework/), todos os créditos ao professor.

## Configurações
* [microframework slim 3 para rotas;](https://www.slimframework.com/)
* [microframework twig 2 para templates;](https://twig.symfony.com/)

## Utilização
```
git clone https://gitlab.com/felipedacs/slig.git <nomeProjeto>
cd <nomeProjeto>
git remote remove origin
git remote add origin <linkRepositorioRemoto>
composer update #atualizar/pegar dependências do projeto
composer dump-autoload -o #atualiza autoload composer
```

## Legendas
* :file_folder: diretório
* :page_facing_up: arquivo
* :pencil2: arquivo que deve ser configurado
* :paperclip: arquivo de exemplo removível


## Árvore de diretórios

* [app](#-app)
 * [controllers](#-controllers)
     * [Controller.php](#-controllerphp)
     * [HomeController.php](#-homecontrollerphp)
 * [daos](#-daos)
     * [Connection.php](#-connectionphp)
     * [DAO.php](#-dao)
 * [functions](#-functions)
     * [helpers.php](#-helpersphp)
     * [twig.php](#-twigphp)
 * [models](#-models)
     * [Load.php](#-loadphp)
 * [traits](#-traits)
     * [View.php](#-viewphp)
 * [uploads](#-uploads)
     * [teste.csv](#-testecsv)
 * [views](#-views)
     * [cache](#-cache)
     * [master.html](#-masterhtml)
     * [home.html](#-homehtml)
* [public](#-public)
  * [assets](#-assets)
     * [css](#-css)
          * [style.css](#-stylecss)
     * [imgs](#-imgs)
          * [slig.png](#-sligpng)
     * [js](#-js)
          * [main.js](#-mainjs)
  * [index.php](#-indexphp)
* [vendor](#-vendor)
* [bootstrap.php](#-bootstrapphp)
* [composer.json](#-composerjson)
* [composer.lock](#-composerlock)
* [.gitignore](#-gitignore)
* [README.md](#-readmemd)



## Explicação

### :file_folder: projeto-folder
>## :file_folder: app
Diretório referente ao core do sistema, com a maioria dos arquivos php

>>### :file_folder: controllers
Diretório de controllers, sempre usar ``namespace app\controllers;``

>>>#### :page_facing_up: Controller.php
Classe que deve ser herdada por todas com ``extends Controller``

>>>#### :page_facing_up: :paperclip: HomeController.php
* exemplo de controller

>>### :file_folder: daos
Diretório de classes que se relacionam com o banco de dados, sempre usar ``namespace app\daos;``

>>>#### :page_facing_up: :pencil2: Connection.php
Classe estática de conexão PDO, padrão singleton ``Connection::getInstance();``
* configuração banco de dados

>>>#### :page_facing_up: DAO.php
Classe que contem as configurações gerais de uso de tabela e extende a Connection.php. As demais classes que trabalham com o banco de dados devem herdar esta classe com ``extends DAO``

>>>#### :page_facing_up: :paperclip: ExemploDAO.php
* Exemplo de extensão da classe DAO

>>### :file_folder: functions
Funções globais já cadastradas no composer.json

>>>#### :page_facing_up: helpers.php
Funções helpers

>>>#### :page_facing_up: twig.php
Funções twig para usar dentro dos templates html

>>#### :file_folder: models
Diretório de classes, sempre usar ``namespace app\models;``

>>>##### :page_facing_up: Load.php
Classe estática responsável por pesquisar se determinado arquivo existe e requerí-lo

>>#### :file_folder: traits
Folder de traits para organizar as mesmas, sempre usar ``namespace app\traits;``

>>>##### :page_facing_up: View.php
Trait referente a configuração do Twig. Quando o código estiver em produção basta descomentar a configuração do cache.
``` php
'cache' => '../app/views/cache',
```

>>#### :file_folder: uploads
Folder de uploads, utilizado pela function "trataUploads" em [helpers.php](#-helpersphp).

>>>##### :page_facing_up: :paperclip: teste.csv
Arquivo csv exemplo.

>>#### :file_folder: views
Folder de templates para a utilização do twig. Os templates podem ser divididos em na pasta views ou em subpastas, por exemplo: views/admin/[template] e views/user/[template]

>>>##### :file_folder: cache
Diretório criado dinamicamente para cache do twig quando configurado em [View.php](#-viewphp).

>>>##### :page_facing_up: :pencil2: master.html
Template geral para utilização em outras Views
* exemplo com estilos no head e script no body

>>>##### :page_facing_up: :paperclip: home.html
Exemplo de view com master
* exemplo com body

>### :file_folder: public
Diretório referente à pasta publica do servidor

>>#### :file_folder: assets
Diretório referente às pasta de arquivos estáticos

>>>##### :file_folder: css
Diretório referente aos arquivos css

>>>>#### :page_facing_up: :paperclip: style.css
* exemplo css

>>>##### :file_folder: imgs
Diretório referente às imgs

>>>>#### :page_facing_up: :paperclip: slig.png
* logo slig

>>>##### :file_folder: js
Diretório referente aos arquivos js

>>>>#### :page_facing_up: :paperclip: main.js
* exemplo js

>>#### :page_facing_up: :pencil2: index.php
Arquivo com as rotas
* exemplo com home

>### :file_folder: vendor
Diretório do composer, ele não sobe por padrão com o .gitignore

>### :page_facing_up: bootstrap
Arquivo base de inicialização com vendor e slim. Quando o código estiver em produção pode ser descomentada a seção "Código em produção", liberando o trecho ``$app = new App;``. Mas também deve-se comentar a seção "Código em desenvolvimento".


>### :page_facing_up: composer.json
Arquivo de configuração do composer.
```
composer update #atualizar/pegar dependências do projeto
composer dump-autoload -o #atualiza autoload composer
```

>### :page_facing_up: composer.lock
Arquivo do composer

>### :page_facing_up: .gitignore
Arquivo que ignora o diretorio vendor no git

>### :page_facing_up: :pencil2: README.md
Arquivo de apresentação do projeto.
1. Apagar a explicação do template, da primeira à ultima tesoura :scissors:
2. Descomentar o template da apresentação e substituir o emoji :question: [legenda] com a informação correta

****************

## :bulb: Dicas
* Abrir pasta public por defaul no servidor embutido do php
```
php -S localhost:8888 -t public #inicia o servidor php na pasta public
```

:scissors:

<!--
# :question: [nome do projeto]
## Descrição
:question: [descrição]

## Dependências
| Nome | Tipo | Versão |
|:---------:|:--------:|:--------:|
| Slim      |:question:|:question:|
| Twig      |:question:|:question:|

## Primeira inicialização
```
composer update #atualizar/pegar dependências do projeto
composer dump-autoload -o #atualiza autoload composer
php -S localhost:8888 -t public #inicia o servidor php na pasta public
```

## Demais inicializações
```
php -S localhost:8888 -t public #inicia o servidor php na pasta public
```
-->
